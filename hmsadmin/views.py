from django.shortcuts import redirect, render
import mysql.connector as mysql
from requests import request
from .models import img
from django.core.files.storage import FileSystemStorage
import plotly
import plotly.graph_objs as go
import pandas as pd
import io
import matplotlib.pyplot as plt
import base64
import csv
import numpy as np
from sklearn import linear_model
import pyqrcode

# import pathlib

# # function to return the file extension
# file_extension = pathlib.Path('my_file.txt').suffix
# print("File Extension: ", file_extension)


study = linear_model.LinearRegression()
# study.fit(df['subject'],df['feelings'])
# print(study.predict(0.3,35))

con = mysql.connect(
    host='localhost',
    user='root',
    password='',
    database='hotel_ms'
)
mycursor = con.cursor()

s = "https://www.youtube.com/channel/UCeO9hPCfRzqb2yTuAn713Mg"

# Generate QR code
url = pyqrcode.create(s)
url.svg("myyoutube.svg", scale=8)


# Create your views here.


def fig_to_base64(fig):
    img = io.BytesIO()
    fig.savefig(img, format='png',bbox_inches='tight')
    img.seek(0)
    return base64.b64encode(img.getvalue())


def roomimg(img_name, room_no):
    sq = "insert into room_img_tbl(image_name,room_number)values(%s,%s)"
    va = (img_name, room_no)
    mycursor.execute(sq, va)
    con.commit()
    print("Image inserted")


def dbroom(room_no, category, prix, img_url):
    rm_status = 0
    sql = "insert into room_tbl(room_number,room_category,room_price,room_status,room_image)values(%s,%s,%s,%s,%s)"
    val = (room_no, category, prix, rm_status, img_url)
    mycursor.execute(sql, val)
    con.commit()
    # roomimg(img,no)
    print("inserted")


def addfoods(prix, food_name, food_cat, food_imgurl):
    sql = "insert into food_tbl(food_price,food_name,food_category,food_image)values(%s,%s,%s,%s)"
    value = (prix, food_name, food_cat, food_imgurl)
    mycursor.execute(sql, value)
    con.commit()
    print("food added")


def dashboard(request):
    sel = "select * from room_tbl where room_status=0"
    mycursor.execute(sel)
    res = mycursor.fetchall()

    # a = np.array([x[0] for x in res]).reshape(1,1)
    # b = np.array([x[3] for x in res]).reshape(1,1)


    # study.fit(a,b)
    # print(study.predict(22,3400))
    arr = [x[3] for x in res]

    pred = round(np.average(arr))
    print(pred)



    s = pd.Series([1, 2, 3])
    fig, ax = plt.subplots()
    s.plot.pie()
    # fig.savefig('my_plot.png')

    encoded = fig_to_base64(fig)
    my_html = '<img src="data:image/png;base64, {}">'.format(
        encoded.decode('utf-8'))

    return render(request,'dashboard.html', {"graph": my_html,"prediction":pred})


def login(request):
    err = {'sms': " "}
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')
        mycursor.execute("select * from admin_tbl")
        results = mycursor.fetchall()
        for m in results:
            if m[3] == email and password == m[5]:
                print("present")
                return redirect('/hms-admin/dashboard/')
            else:
                print("abscent")
                err = {'sms': "Invalid email or password"}

    return render(request, 'login.html', err)


def addroom(request):
    if request.method == 'POST':
        room_no = request.POST.get('room_no')
        room_type = request.POST.get('room_type')
        room_prix = request.POST.get('room_prix')
        room_img = request.FILES['room_img']
        fs = FileSystemStorage()
        filename = fs.save(room_img.name, room_img)
        img_url = "http://127.0.0.1:8000" + fs.url(filename)

        dbroom(room_no, room_type, room_prix, img_url)
    return render(request, 'room.html')


def addfood(request):
    if request.method == 'POST':
        foodtype = request.POST.get("foodtype")
        food_name = request.POST.get("food_name")
        foodPrice = request.POST.get("foodPrice")
        foodPicture = request.FILES["foodPicture"]
        # pd = img()
        # pd.Img = request.FILES["foodPicture"]
        fs = FileSystemStorage()
        print(foodPicture.size)
        print(foodPicture.content_type)
        name = fs.save(foodPicture.name, foodPicture)
        img_url = "http://127.0.0.1:8000"+fs.url(name)
        # print(img_url)

        # pd.save()
        # print(foodPicture)
        addfoods(foodPrice, food_name, foodtype, img_url)
    return render(request, 'food.html')

def checkout(request):
    return render(request,"checkout.html")

def booked(request):
    if request.method == "POST":
        room_id = request.POST.get("room_id")
        print(room_id)
        update = "update room_tbl set room_status= 0 where room_id="+room_id
        mycursor.execute(update)
        con.commit()

    sel = "select * from room_tbl where room_status=1"
    mycursor.execute(sel)
    res = mycursor.fetchall()
    
    return render(request,'booked.html',{'data':res})

def order(request):
    if request.method == "POST":
        food_id = request.POST.get("food_id")
        print(food_id)
        update = "update food_tbl set status=0 where food_id="+food_id
        mycursor.execute(update)
        con.commit()

    sel = "select * from food_tbl where status=1"
    mycursor.execute(sel)
    res = mycursor.fetchall()
    return render(request,"order.html",{"data":res})


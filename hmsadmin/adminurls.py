
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path
from hmsadmin import views

urlpatterns = [
    path('',views.login ,name="login"),
    path('dashboard/',views.dashboard , name="dashboard"),
    path('food/',views.addfood,name="food"),
    path('room/',views.addroom,name="room"),
    path('checkout/',views.checkout,name="checkout"),
    path('booked/',views.booked,name="booked"),
    path('order/',views.order,name="order"),
    
]
if settings.DEBUG:
    urlpatterns+= static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
    )

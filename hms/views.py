from turtle import update
from unittest import result
from urllib import response
from django.shortcuts import render
import mysql.connector as mysql
import csv
import requests
import os
import phonenumbers
from phonenumbers import geocoder
from phonenumbers import carrier



con = mysql.connect(
    host='localhost',
    user = 'root',
    password = '',
    database = 'hotel_ms'
)
# Create your views here.

mycursor = con.cursor()


def chatroom(qn,answer):
    get = "insert into chat(ai_agent,customer)values(%s,%s)"
    val = (answer,qn)
    mycursor.execute(get,val)
    con.commit()
    print("chat added")

def pay_room(mode,customer_id,room_id,days):
    get = "insert into customer_room(customer_id,room_number,mode,days)values(%s,%s,%s,%s)"
    val = (customer_id,room_id,mode,days)
    mycursor.execute(get,val)
    con.commit()
    print("payment approved added")

    update = "update room_tbl set room_status= 1 where room_id="+room_id
    mycursor.execute(update)
    con.commit()

def pay_food(mode,customer_id,food_id):
    get = "insert into customer_food_tbl(customer_id,food_id,mode)values(%s,%s,%s)"
    val = (customer_id,food_id,mode)
    mycursor.execute(get,val)
    con.commit()
    print("payment approved added")

    update = "update food_tbl set status=1 where food_id="+food_id
    mycursor.execute(update)
    con.commit()
    


def user(firstname,surname,email,contact,credit_card,country,company):
    q = "insert into customers_tbl(firstName,surName,customer_email,contact,credit_card,country,company)values(%s,%s,%s,%s,%s,%s,%s)"
    v = (firstname,surname,email,contact,credit_card,country,company)
    mycursor.execute(q,v)
    con.commit()
    print("inserted")

# Create your views here.
def index(request):
    if request.method=="POST":
        mode = request.POST.get("mode")
        room_id = request.POST.get("room_id")
        days = request.POST.get("days")
        customer_id = "1"
        print(mode)
        print(room_id)
        print(days)
        
        pay_room(mode,customer_id,room_id,days)

    sel = "select * from room_tbl where room_status=0"
    mycursor.execute(sel)
    res = mycursor.fetchall()
    return render(request,'index.html',{'data':res})

def restuarant(request):
    if request.method =="POST":
        mode = request.POST.get("mode")
        food_id = request.POST.get("food_id")
        customer_id = "1"
        pay_food(mode,customer_id,food_id)
        
    sel = "select * from food_tbl where status=0"
    mycursor.execute(sel)
    res = mycursor.fetchall()
    return render(request,'restuarant.html',{"food":res})

def aboutus(request):
    data = requests.get("http://localhost/api/background.php")
    data2 = requests.get("http://localhost/api/details.php")
    res = data.json()
    res2 = data2.json()
    # print(res["sms"][1])
    print(res2["data"])
    return render(request,'aboutus.html',{"info":res,"info2":res2})

def chat(request):
    res =""

    return render(request,"chat.html",{"data":res})

def signin(request):
    mismatch = ""
    if request.method == "POST":
        firstname = request.POST.get("first_name")
        surname = request.POST.get("surname")
        email = request.POST.get("email")
        contact = request.POST.get("contact")
        credit_card = request.POST.get("credit_card")

        number = '+256781260856'
        ch_number = phonenumbers.parse(number, "CH")
        country = geocoder.description_for_number(ch_number, "en")
        service_provider = phonenumbers.parse(number, "RO")
        company = carrier.name_for_number(service_provider, "en")

        user(firstname,surname,email,contact,credit_card,country,company)

    return render(request,'signin.html')

def chatbrain(request):
    if request.method == 'GET':
        search = request.GET.get("q")
        def dialog(t):
            with open("D:/python/final year project/endyr/hms/tweets.csv") as file:
                capture = csv.reader(file)
                for x in capture:
                    if x[0] in t:
                        return x[2]
        answer = dialog(search)
        chatroom(search,answer)
        

        print(answer)
      
        print(search)
    sel = "select * from chat order by id "
    mycursor.execute(sel)
    res = mycursor.fetchall()
    return render(request,"chatbrain.html",{"data":res})
from django.contrib import admin
from django.urls import path
from hms.views import chat, chatbrain, index , restuarant ,aboutus,signin

urlpatterns = [
    path('',index),
    path('restuarant/',restuarant),
    path('aboutus/',aboutus),
    path('chat/',chat),
    path('signin/',signin),
    path('chatbrain/',chatbrain),
]

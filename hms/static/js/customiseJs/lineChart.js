google.charts.load("current", { packages: ["corechart"] });
google.charts.setOnLoadCallback(drawVisualization);

function drawVisualization() {
	// Some raw data (not necessarily accurate)
	var data = google.visualization.arrayToDataTable([
		[
			"China",
			"Malawi",
			"England",
			"Newyork",
			"USA",
			"Rwanda",
			"Canada",
		],
		["2019/05", 165, 938, 522, 998, 450, 614.6],
		["2020/06", 135, 1120, 599, 1268, 288, 682],
		["2021/07", 157, 1167, 587, 807, 397, 623],
		["2021/12", 139, 1110, 615, 968, 215, 609.4],
		["2022/02", 136, 691, 629, 1026, 366, 569.6],
	]);

	var options = {
		title: "Transactions made based on Countries",
		vAxis: { title: "Countries" },
		hAxis: { title: "Month" },
		seriesType: "bars",
		series: { 5: { type: "line" } },
	};

	var chart = new google.visualization.ComboChart(
		document.getElementById("chart_div")
	);
	chart.draw(data, options);
}

google.charts.load("current", { packages: ["corechart"] });
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
	var data = google.visualization.arrayToDataTable([
		["Transport", "Hours per Day"],
		["Allowance", 11],
		["Workers meals", 2],
		["Electricity", 2],
		["Water", 2],
		["Security", 7],
	]);

	var options = {
		title: "Monthly expenditure",
		is3D: true,
	};

	var chart = new google.visualization.PieChart(
		document.getElementById("piechart_3d")
	);
	chart.draw(data, options);
}
